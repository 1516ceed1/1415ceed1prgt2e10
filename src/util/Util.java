
package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Paco Aldarias Raya
 */


public class Util {

  public int pedirInt() throws IOException {

    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea;
    int numero;
      linea = buffer.readLine();
      numero = Integer.parseInt(linea);
      
    return numero;

  }

  public String pedirString() {

    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    String linea = null;
    try {

      linea = buffer.readLine();
    } catch (Exception e) {
    }
    return linea;

  }

}
