
/**
 * Fichero: Constructor.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */

public class Constructor {

  private int atributo;

  Constructor() {
    System.out.println("Constructor sin");
  }

  Constructor(int parametro) {
    System.out.println("Constructor con");
  }

  public static void main (String args[]) {
    Constructor e1=new Constructor();
    Constructor e2=new Constructor(1);
  }

/* EJECUCION:
Constructor sin
Constructor con
*/
  
}
