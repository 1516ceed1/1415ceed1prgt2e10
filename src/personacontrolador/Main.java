package personacontrolador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import personamodelo.Persona;
import personavista.Vista;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException  {
        // TODO code application logic here
        
        Persona persona;
        Vista vista = new Vista();
        persona = vista.obtenerPersona();
        vista.mostrarPersona(persona);
        
        
        
    }

    
}
