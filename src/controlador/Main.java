package controlador;

import java.io.IOException;
import modelo.Alumno;
import vista.Vista;

/**
 * Práctica Obligatoria Tema 3
 *
 * @author Paco Aldarias Raya
 * @date 18/10/2013
 */
public class Main {

   public static void main(String[] args) throws IOException {

      /*
       Clase Main
       Se encargara del control del programa
       Estara la funcion principal
       Importara las clases de los paquetes de VISTA y MODELO
       */
      Vista vista;

      Alumno p;

      /*
       Creamos un objeto con el modelo
       */
      p = new Alumno();

      /*
       Creamos un objeto vista coger y mostrar datos
       */
      vista = new Vista();

      /*
       Cogemos los datos con la vista
       */
      p = vista.tomaDatos();

      /*
       Mostamos los datos con la vista
       Como muestra datos es estatic no hay que crear objetos.
       */
      Vista.muestraDatos(p);

   }
}
