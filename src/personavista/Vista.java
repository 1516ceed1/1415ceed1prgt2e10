/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package personavista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import personamodelo.Persona;

/**
 *
* Fichero: Vista.java
* @author Paco Aldarias <paco.aldarias@ceedcv.es>
* @date 13-oct-2015
*/


 
public class Vista {
    
    
public   Persona obtenerPersona() throws IOException{

    Persona persona = new Persona();
        
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        
        System.out.print("Nombre: ");
        String linea = br.readLine();
        persona.setNombre(linea);
        
        System.out.print("Edad: ");
        linea = br.readLine();
        int edad = Integer.parseInt(linea);
        persona.setEdad(edad);
        
        return persona;
    }

    public void mostrarPersona(Persona persona) {
        System.out.println("Nombre: "+persona.getNombre());
        System.out.println("Edad: "+persona.getEdad());        
    }
    
}
