/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class PersonaMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException  {
        // TODO code application logic here
        
        Persona persona = new Persona();
        
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        
        System.out.print("Nombre: ");
        String linea = br.readLine();
        persona.setNombre(linea);
        
        System.out.print("Edad: ");
        linea = br.readLine();
        int edad = Integer.parseInt(linea);
        persona.setEdad(edad);
        
        System.out.println("Nombre: "+persona.getNombre());
        System.out.println("Edad: "+persona.getEdad());
        
    }

    
}
