package modelo;

/**
 * @author Paco Aldarias Raya
 */
public class Alumno {

  /*
   Clase Alumno
   Proporciona los constructores para crear Personas
   */
  private String nombre;
  private int edad;

  public Alumno() {
    nombre = "Paco";
    edad = 25;
  }

  public Alumno(String n, int e) {
    nombre = n;
    edad = e;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the edad
   */
  public int getEdad() {
    return edad;
  }

  /**
   * @param edad the edad to set
   */
  public void setEdad(int edad) {
    this.edad = edad;
  }

}
