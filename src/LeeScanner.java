
import java.util.Scanner;

/**
 * @author Paco Aldarias<paco.aldarias@ceedcv.es>
 * @author 15-oct-2015
 */
public class LeeScanner {

   public static void main(String[] args) {

      String nombre;
      int horas;
      double pagoPorHora, pagoTotal;

      Scanner teclado = new Scanner(System.in);

      System.out.print("Como te llamas ? ");
      nombre = teclado.nextLine();

      System.out.print("Cuantas horas trabajaste esta semana ? ");
      horas = teclado.nextInt();

      System.out.print("Cuanto te pagan por hora ? ");
      pagoPorHora = teclado.nextDouble();
      pagoTotal = horas * pagoPorHora;

      System.out.println("Hola " + nombre);
      System.out.println("Tu sueldo es " + pagoTotal);
   }

}

/* Ejecucion:
 Como te llamas ? Paco
 Cuantas horas trabajaste esta semana ?5
 Cuanto te pagan por hora ?2
 Hola  Paco
 Tu sueldo es  10.0
 */
